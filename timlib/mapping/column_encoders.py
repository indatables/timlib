from sklearn import preprocessing
import pandas as pd
import numpy as np

import unicodedata

class LabelInstance:
    def __init__(self, comparator, instance):
       self.comparator = comparator 
       self.original = instance

    def __repr__(self):
        return self.original

    def __eq__(self, y):
        if type(y) == str:
            return self.comparator.eq(self.original, y)
        else:
            return self.comparator.eq(self.original, y.original)

class LabelComparator:
    def __init__(self, caps='no', unicode=False, strip_chars=None, 
                 part_synonyms=None, label_synonyms=None):
        """
        Args:
            caps (str): One of {'no', 'only_first', 'all'} Indicating 
                if caption of letters should be compared.
            unicode (bool): Indicating if labels should be normalized 
                to ascii befrore comparission.
            strip_chars: One of {False, None, [...]} indicating if 
                characters should be stripted from the labels before 
                comparision. When None alle white space will be 
                striped.
            part_synonyms (dict[str,list[str]]): Defining which parts 
                of labels are a synonym for an other part.
            label_synonyms (dict[str,list[str]]): Defining which 
                labels are actulaly equal, for instance when comparing 
                labels written in different languages.
        """
        self.pipeline = []
        
        if caps == 'no':
            self.pipeline.append(str.lower)
        elif caps == 'only_first':
            self.pipeline.append(
                lambda x: str.lower(x[1:])
            )
        
        if not unicode:
            self.pipeline.append(
                lambda x: unicodedata.normalize('NFKD', x)
                              .encode('ascii', 'ignore')
                              .decode()
            )

        if strip_chars != False:
            self.pipeline.append(
                lambda x: str.strip(x, strip_chars)
            )

        if part_synonyms:
            for part, synonyms in part_synonyms.items():
                for synonym in synonyms:
                    self.pipeline.append(
                        lambda x, synonym=synonym, part=part
                            : str.replace(x, synonym, part)
                    )
        
        
        self._label_synonyms_ser = None
        if label_synonyms:
            ser_list = []
            for label, synonyms in label_synonyms.items():
                ser_list.append(pd.Series([label]*len(synonyms), 
                                          index=synonyms)
                                )
            self._label_synonyms_ser = pd.concat(ser_list)    

    def normalize(self, x):
        r = x
        for step in self.pipeline:
            r = step(r)
        if self._label_synonyms_ser is not None:
            r = self._label_synonyms_ser.get(r,default=r)
        return r 
    
    def normalize_series(self, ser):
        r = ser
        for step in self.pipeline:
            r = r.map(step)
        if self._label_synonyms_ser is not None:
            is_synonym = r.map(self._label_synonyms_ser.index.contains)
            r[is_synonym] = r[is_synonym].map(self._label_synonyms_ser)
        return r
    
    def eq(self, x, y):
        return self.normalize(x) == self.normalize(y)
    
    def make_instance(self, instance_str):
        return LabelInstance(self, instance_str) 


class BaseEncoder(object):
    def __init(self):
        pass
    
    def fit(self, y):
        raise NotImplementedError(
                "fit() method is not yet implemented for {}."
                .format(type(self))
            )
    
    def transform(self, y):
        raise NotImplementedError(
                "transform() method is not yet implemented for {}."
                .format(type(self))
            )
    
    def fit_transform(self, y):
        self.fit(y)
        return self.transform(y)
    
    def inverse_transform(self, y):
        raise NotImplementedError(
                "inverse_transform() method is not yet implemented for {}."
                .format(type(self))
            )


class LabelColumnEncoder(BaseEncoder):
    def __init__(self, comparator='strict'):
        self.le = preprocessing.LabelEncoder()
        self.used = False
        if comparator == 'fuzzy':
            self.comparator = LabelComparator(
                    part_synonyms={'_': ['\t','\n','-','+','=','.',',',' ']}, 
                    label_synonyms=None, 
                    caps='no', 
                    unicode=False, 
                    strip_chars=None,
                )
        elif comparator == 'strict':
            self.comparator = LabelComparator(
                    part_synonyms=None, 
                    label_synonyms=None, 
                    caps='all', 
                    unicode=True, 
                    strip_chars=False,
                )
        else:
            self.comparator = comparator

    def fit(self, y):
        if self.used:
            raise NotImplementedError(
                    ('Not allowed to fit after transform. Fit will possibly '
                     'change the order of the labels, which makes the previous '
                     'transform invalid. For this functinalty use the '
                     'IncrementalLabelColumnEncoder class.')
                )
        
        y_normalized = self.comparator.normalize_series(y)
        if hasattr(self.le, "classes_"):
            # If already fitted on other data, use these labels also in the new
            # fit.
            y_normalized = np.concatenate([self.le.classes_, y_normalized])
        self.le.fit(y_normalized)

    def transform(self, y):
        self.used = True
        y_normalized = self.comparator.normalize_series(y)
        return self.le.transform(y_normalized)

class EnumColumnEncoder(BaseEncoder):
    
    def __init__(self, classes_values, extra_out_of_values_class=False):
        """
        Args:
             classes_values (iterable of iterables): Deffines the values
                 corresponding to each class. The length of the first 
                 iterable deffines the number of classes.
             extra_out_of_values_class (bool): When False, raises
                 an exception when values occur not in classes_values. 
                 When True, makes one extra class for values which are
                 not in classes_values.
        """
        ser_list = []
        for class_number, class_values in enumerate(classes_values):
            ser_list.append(
                    pd.Series(class_number, index=class_values)
                )
        self._label_mapper_ser = pd.concat(ser_list)
        if extra_out_of_values_class:
            self._out_of_values_class = len(classes_values)
        else:
            self._out_of_values_class = None

    def transform(self, y):
        if self._out_of_values_class is not None:
            if type(y) == pd.Series:
                y_unique = y.unique()
            else:
                print('Warning: When using an extra_out_of_values_class '
                      'transform() is much faster when used with a '
                      'pandas.Series')
                y_unique = np.unique(y)

            self._label_mapper_ser = (
                self._label_mapper_ser.reindex(
                    self._label_mapper_ser.index.union(y_unique), 
                    fill_value=self._out_of_values_class,
                )
            )
        if type(y) == pd.Series:
            # When we have a series we can use the faster map method.
            result = y.map(self._label_mapper_ser)
        else:
            # Else, encoded labels are selected using the original labels  as 
            # index.
            result = self._label_mapper_ser[y] 
        if self._out_of_values_class is None:
            if result.isna().any():
                raise ValueError("Out of class values exists.")
        return result


class BoolColumnEncoder(EnumColumnEncoder):

    def __init__(self, true_values=None, false_values=None):
        """
        Args:
            true_values (None, bool, String, list): Deffines what 
                values will be considderd as True. If None,
                false_values must be given and all non false_values
                will be considered as True.
            false_values (None, bool, String, list): Deffines what 
                values will be considderd as False. If None, 
                true_values must be given and all non true_values will
                be considered as False.
        """
        if true_values is None and false_values is None:
            raise Exception('true_values and false_values can not both be None.')

        if true_values is None:
            true_values = []
            out_of_value_class = 1
        elif false_values is None:
            false_values = []
            out_of_value_class = 0
        else:
            out_of_value_class = None
        
        super().__init__(
            classes_values=[false_values, true_values], 
            extra_out_of_values_class=out_of_value_class is not None,
        )
        self._out_of_values_class = out_of_value_class
       


