Type Inferencing and Mapping Library
===============================

version number: 0.0.1
authors: Stan van Lier; Riccardo Tiebax

Overview
--------

A Python package for inferencing user defined column types in data and for mapping non-numeric columns to numeric columns.

Installation / Usage
--------------------

To install use pip:

    $ pip install timlib


Or clone the repo:

    $ git clone https://github.com/indatables/timlib.git
    $ python setup.py install

Dependency management
------------------
    
Contributing
------------

TBD

Example
-------

TBD